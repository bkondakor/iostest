//
//  FirstViewController.swift
//  test
//
//  Created by Student on 2016. 10. 04..
//  Copyright © 2016. Student. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate {

    // MARK: Properties
    @IBOutlet weak var newItemField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var enableToggle: UISegmentedControl!
    @IBOutlet weak var lastText: UITextView!
    @IBOutlet weak var wholeText: UITextView!
    
    var todoList = ""
    var newItem:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        newItemField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        newItemField.resignFirstResponder()
        addNewItem()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        newItem = textField.text!
        textField.text = ""
    }
    
    
    // MARK: Actions
    @IBAction func clearList(_ sender: UIButton) {
        todoList = ""
        lastText.text = ""
        wholeText.text = todoList
        
    }
    
    @IBAction func add(_ sender: UIButton) {
        newItemField.resignFirstResponder()
        addNewItem()
    }
    
    func addNewItem() {
        if let text = newItem , !text.isEmpty {
            todoList.append("* " + text + "\n")
            lastText.text = text
            wholeText.text = todoList
        }
    }
    
    @IBAction func switchToggle(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex
        {
        case 0:
            newItemField.isEnabled = true
            addButton.isEnabled = true
        case 1:
            newItemField.isEnabled = false
            addButton.isEnabled = false
        default:
            break;
            
        }
        
    }
}

