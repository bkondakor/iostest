//
//  SimpleAppUITests.swift
//  SimpleAppUITests
//
//  Created by Student on 2016. 10. 07..
//  Copyright © 2016. Student. All rights reserved.
//

import XCTest

class SimpleAppUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testList(){
        
        let app = XCUIApplication()
        let newitemTextField = app.textFields["newItem"]
        newitemTextField.tap()
        newitemTextField.typeText("hogy")
        app.buttons["addButton"].tap()
        newitemTextField.tap()
        newitemTextField.typeText("hogy")
        app.buttons["addButton"].tap()
        XCTAssertEqual(app.textViews["wholeText"].value as? String, "* hogy\n* hogy\n", "Something is wrong")
    }
    
    func testLastItem(){
        
        let app = XCUIApplication()
        let newitemTextField = app.textFields["newItem"]
        newitemTextField.tap()
        newitemTextField.typeText("hogy")
        app.buttons["addButton"].tap()
        newitemTextField.tap()
        XCTAssertEqual(app.textViews["lastItem"].value as? String, "hogy", "Something is wrong")
        newitemTextField.typeText("hogy hogy")
        app.buttons["addButton"].tap()
        XCTAssertEqual(app.textViews["lastItem"].value as? String, "hogy hogy", "Something is wrong")
    }
    
    func testClear() {
        let app = XCUIApplication()
        let newitemTextField = app.textFields["newItem"]
        newitemTextField.tap()
        newitemTextField.typeText("hogy")
        app.buttons["addButton"].tap()
        XCTAssertEqual(app.textViews["wholeText"].value as? String, "* hogy\n", "Something is wrong")
        app.buttons["clearButton"].tap()
        XCTAssertEqual(app.textViews["wholeText"].value as? String, "", "Something is wrong")
        XCTAssertEqual(app.textViews["lastItem"].value as? String, "", "Something is wrong")
    }
    
    func testToggle(){
        
        let app = XCUIApplication()
        let newitemTextField = app.textFields["newItem"]
        let button = app.buttons["addButton"]
        XCTAssert(newitemTextField.isEnabled)
        app.buttons["Disable"].tap()
        XCTAssertFalse(newitemTextField.isEnabled)
        XCTAssertFalse(button.isEnabled)
        app.buttons["Enable"].tap()
        XCTAssert(newitemTextField.isEnabled)
        XCTAssert(button.isEnabled)
    }
    
    
}
